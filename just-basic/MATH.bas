CALL TESTALL

REM **********************************
REM Adds two numbers
REM **********************************
FUNCTION ADD(X, Y)
    ADD = X + Y
END FUNCTION

REM **********************************
REM Find sum of first N numbers
REM **********************************
FUNCTION SUMOFFIRST(N)
    SUMOFFIRST = SUMREC(N, 0, 0)
END FUNCTION


REM **********************************
REM Recursive function used by SUMOFFIRST
REM **********************************
FUNCTION SUMREC(N, I, ACC)
    IF I = N THEN
        SUMREC = ACC + I
    ELSE
        SUMREC = SUMREC(N, I + 1, ACC + I)
    END IF
END FUNCTION

REM **********************************
REM Find sum of first N numbers by looping
REM **********************************
FUNCTION SUMIMPER(N)
    SUM = 0
    FOR I = 1 TO N
        SUM = SUM + I
    NEXT I
    SUMIMPER = SUM
END FUNCTION

REM **********************************
REM Find nth Triangular number
REM **********************************
FUNCTION T(N)
    T = SUMOFFIRST(N)
END FUNCTION

REM **********************************
REM Find first Fibonacci numbers
REM **********************************
FUNCTION F(N)
    IF N = 1 THEN
        F = 0
    ELSE
        IF  N = 2 THEN
            F = 1
        ELSE
            F = F(N - 1) + F(N - 2)
        END IF
    END IF
END FUNCTION

REM **********************************
REM Test Subroutines
REM **********************************
SUB TESTALL
    CALL TESTADD
    CALL TESTSUMOFFIRST
    CALL TESTSUBIMPER
    CALL TESTT
    CALL TESTF
END SUB

SUB TESTADD
    LET A = ADD(3,4)
    IF A = 7 THEN
        PRINT "ADD(3,4): PASS"
    ELSE
        PRINT "ADD(3,4): FAIL"
    END IF
END SUB

SUB TESTSUMOFFIRST
    LET X = SUMOFFIRST(3)
    IF X = 6 THEN
        PRINT "SUMOFFIRST(3): PASS"
    ELSE
        PRINT "SUMOFFIRST(3): FAIL"
    END IF
END SUB

SUB TESTSUBIMPER
    LET X = SUMIMPER(4)
    IF X = 10 THEN
        PRINT "SUMIMPER(4): PASS"
    ELSE
        PRINT "SUMIMPER(4): FAIL"
    END IF
END SUB

SUB TESTT
    LET X = T(4)
    IF X = 10 THEN
        PRINT "T(4): PASS"
    ELSE
        PRINT "T(4): FAIL"
    END IF
END SUB

SUB TESTF
    LET X = F(4)
    IF X = 2 THEN
        PRINT "F(4): PASS"
    ELSE
        PRINT "F(4): FAIL"
    END IF
END SUB
