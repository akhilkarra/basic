CALL TEST

REM **********************************
REM Find sum of first N numbers
REM **********************************
FUNCTION SUMOFFIRST(N)
    SUMOFFIRST = SUMREC(N, 0, 0)
END FUNCTION

REM **********************************
REM Recursive function used by SUMOFFIRST
REM **********************************
FUNCTION SUMREC(N, I, ACC)
    IF I = N THEN
        SUMREC = ACC + I
    ELSE
        SUMREC = SUMREC(N, I + 1, ACC + I)
    END IF

END FUNCTION

REM **********************************
REM Find sum of first N numbers by looping
REM **********************************
FUNCTION SUMIMPER(N)
    SUM = 0
    FOR I = 1 TO N
        SUM = SUM + I
    NEXT I
    SUMIMPER = SUM
END FUNCTION

REM **********************************
REM Find nth Triangular number
REM **********************************
FUNCTION T(N)
    T = SUMOFFIRST(N)
END FUNCTION

REM **********************************
REM Test Subroutines
REM **********************************

SUB TEST
    CALL TESTSUMOFFIRST
    CALL TESTSUBIMPER
    CALL TESTT
END SUB

SUB TESTSUMOFFIRST
    LET X = SUMOFFIRST(3)
    IF X = 6 THEN
        PRINT "SUMOFFIRST(3): PASS"
    ELSE
        PRINT "SUMOFFIRST(3): FAIL"
    END IF
END SUB

SUB TESTSUBIMPER
    LET X = SUMIMPER(4)
    IF X = 10 THEN
        PRINT "SUMIMPER(4): PASS"
    ELSE
        PRINT "SUMIMPER(4): FAIL"
    END IF
END SUB

SUB TESTT
    LET X = T(4)
    IF X = 10 THEN
        PRINT "T(4): PASS"
    ELSE
        PRINT "T(4): FAIL"
    END IF
END SUB



