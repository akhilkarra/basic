CLS

CALL TEST
CALL MAIN

REM **********************************
REM Adds two numbers
REM **********************************
FUNCTION ADD(X, Y)
    ADD = X + Y
END FUNCTION

SUB MAIN
    INPUT "ENTER X: "; X
    INPUT "ENTER Y: "; Y
    PRINT "X + Y = "; ADD(X, Y)
END SUB

REM **********************************
REM Test Subroutines
REM **********************************
SUB TEST
    CALL TESTADD
END SUB

SUB TESTADD
    LET A = ADD(3,4)
    IF A = 7 THEN
        PRINT "ADD(3,4): PASS"
    ELSE
        PRINT "ADD(3,4): FAIL"
    END IF
END SUB

END

