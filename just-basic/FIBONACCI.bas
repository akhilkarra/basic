
CALL TEST

REM **********************************
REM Find first Fibonacci numbers
REM **********************************

FUNCTION FIBONACCI(N)
    IF N = 1 THEN
        F = 0
    ELSE
        IF  N = 2 THEN
            F = 1
        ELSE
            F = F(N - 1) + F(N - 2)
        END IF
    END IF
END FUNCTION


REM **********************************
REM Test Subroutines
REM **********************************

SUB TEST
    CALL TESTFIB
END SUB

SUB TESTFIB
    LET X = FIBONACCI(4)
    IF X = 2 THEN
        PRINT "FIBONACCI(4): PASS"
    ELSE
        PRINT "FIBONACCI(4): FAIL"
    END IF
END SUB
